<?php

class CurrencyInformer_Widget
{
    private $_rates = [];
    
    public function __construct($rates)
    {
        $this->_rates = $rates;
    }
    
    public function __toString()
    {
        return get_class($this);
    }
    
    public function render()
    {
        $templatePath = explode('_',$this);
        $templatePath[2] = strtolower($templatePath[2]);
        $templatePath = implode('/',$templatePath);
        $templatePath .= '.template.php';
        include $templatePath;
    }
}