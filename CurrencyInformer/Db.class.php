<?php

class CurrencyInformer_Db
{
    protected $_config;
    protected $_connection;

    public function __construct()
    {
        $config = array();
        include(CURRENCY_INFORMER_ROOT . DIRECTORY_SEPARATOR . 'config.php');
        $this->_config = $config['database'];
    }
    
    protected function _connect()
    {
        $dsn = "mysql:dbname={$this->_config['dbname']};host={$this->_config['host']}";

        $this->_connection = new PDO(
            $dsn,
            $this->_config['username'],
            $this->_config['password']
        );
    }

    protected function _getConnection()
    {
        if (null === $this->_connection)
        {
            $this->_connect();
        }
        return $this->_connection;
    }
 
    public function query($query)
    {
        return $this->_getConnection()->query($query);
    }
}