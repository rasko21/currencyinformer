<?php

//дополнительно надо подключить jquery и возможно jqueryui

class CurrencyInformer
{

    const BASIC_URL = 'http://www.nbrb.by/API/ExRates/';

    /*
     * @var object CurrencyInformer_Db $_connection
     */
    protected $_connection;

    /*
     * @var string $_dateToday (format 'Y-m-d')
     */
    protected $_dateToday;
    
    public function __construct()
    {
        //@TODO ZAVESTI AVTOLOAD_REGISTER_PATHS
        
        //Setting today date formatted for comparison with database values
        $this->_dateToday = new DateTime();
        $this->_dateToday = $this->_dateToday->format('Y-m-d');
        
        //Setting database connection
        include('CurrencyInformer/Db.class.php');
        $this->_connection = new CurrencyInformer_Db;
        
        $this->_checkRates();
    }

    public function test()
    {
        $rates = $this->_getRates();
        $this->getWidget('rates', $rates)->render();
    }
    
    /*
     * Checking out for requested rates in database.
     * If not found - calling _importRates() and INSERT data into db
     */
    private function _checkRates($date = null)
    {
        $date = is_null($date)
            ? $this->_dateToday
            : $date;
        
        $countRatesOnDate = $this->_connection
                ->query("SELECT COUNT(*) "
                        . "FROM currency_informer_rates "
                        . "WHERE date = '$date'")
                    ->fetchColumn();
        
        if ($countRatesOnDate == 0)
        {
            $this->_importRates($date);
        }
    }
    
    private function _getRates($date = null)
    {
        /*
         * If date is not today (today is we autochecking data in db on construct) - we check for 
         * data in db. If there`s no data function _checkRates() calls _importRates() 
         */
        if (!is_null($date))
        {
            $this->_checkRates($date);
        }
            
        $date = is_null($date)
            ? $this->_dateToday
            : $date;            
        
        
        $rates = $this->_connection
                ->query("SELECT * "
                        . "FROM currency_informer_rates "
                        . "WHERE date = '$date'")
                    ->fetchAll(PDO::FETCH_ASSOC);

        return $rates;
    }

    /*
     * Importing rates from nbrb API
     */
    protected function _importRates($date)
    {
          
        $date .= 'T00:00:00';
        http://www.nbrb.by/API/ExRates/Rates?onDate=2016-06-30T00:00:00&Periodicity=0

        //$dataFromApi - array of objects    
        $dataFromAPI = $this->curlRequest("Rates/?onDate=$date&Periodicity=0");
        
        //data formatted for SQL INSERT
        $dataSqlFormatted = null;
        foreach ($dataFromAPI as $singleRateObject)
        {
            $singleRate = null;
            $singleRate = "'$singleRateObject->Cur_ID'";
            $singleRate .= ",'$singleRateObject->Date'";
            $singleRate .= ",'$singleRateObject->Cur_Abbreviation'";
            $singleRate .= ",'$singleRateObject->Cur_Scale'";
            $singleRate .= ",'$singleRateObject->Cur_Name'";
            $singleRate .= ",'$singleRateObject->Cur_OfficialRate'";
            
            $dataSqlFormatted .= "($singleRate),";
        }
        $dataSqlFormatted = rtrim($dataSqlFormatted, ',');
        
        $this->_connection
                ->query("INSERT INTO currency_informer_rates VALUES$dataSqlFormatted");
    }

    protected function curlRequest($url)
    {
        $url = self::BASIC_URL . $url;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result);
    }

    public function getWidget($name, $rates)
    {
        $widgetName = "CurrencyInformer_Widget_$name";
        return new $widgetName($rates);
    }
}
