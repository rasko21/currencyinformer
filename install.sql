CREATE TABLE `currency_informer_rates` (
	`id` TINYINT(3) UNSIGNED NOT NULL,
	`date` DATE NOT NULL,
	`abr` CHAR(3) NOT NULL,
	`name` CHAR(30) NOT NULL,
	`rate` FLOAT NOT NULL,
	`scale` INT UNSIGNED NOT NULL COMMENT 'scale name = rate \'BYN\'',
	`period` TINYINT(1) UNSIGNED NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
