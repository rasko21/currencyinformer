<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('CURRENCY_INFORMER_ROOT', getcwd());

    include('CurrencyInformer/CurrencyInformer.class.php');
    include('CurrencyInformer/Widget.class.php');
    include('CurrencyInformer/Widget/Rates.class.php');
    include('CurrencyInformer/Widget/Converter.class.php');
    
    $currencyInformer = new CurrencyInformer();
    $currencyInformer->test();
    
//    $currencyInformer->getWidget('rates')->render();
//    $currencyInformer->getWidget('converter')->render();
?>